Description: Fixes strict epsilons in MatrixTests
 This tweaks an epsilon values in the unit tests that were too strict and
 caused failures on powerpc.
Author: Paul Novotny <paul@paulnovo.us>
Bug-Debian: https://bugs.debian.org/838272
Last-Update: 2016-10-14

--- a/SurgSim/Math/UnitTests/MatrixTests.cpp
+++ b/SurgSim/Math/UnitTests/MatrixTests.cpp
@@ -467,11 +467,11 @@
 	const int SIZE = Matrix::RowsAtCompileTime;
 
 	Matrix a = Matrix::Constant(6.0f);
-	EXPECT_NEAR(6.0f * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(6.0f * SIZE * SIZE, a.sum(), 5e-6);
 	const Matrix b = Matrix::Constant(7.0f);
-	EXPECT_NEAR(7.0f * SIZE * SIZE, b.sum(), 1e-6);
+	EXPECT_NEAR(7.0f * SIZE * SIZE, b.sum(), 5e-6);
 	a = b;
-	EXPECT_NEAR(7.0f * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(7.0f * SIZE * SIZE, a.sum(), 5e-6);
 }
 
 // ==================== ACCESS ====================
@@ -495,9 +495,9 @@
 	for (int i = 0;  i < SIZE;  ++i)
 	{
 		Vector rowVector = a.row(i);
-		EXPECT_NEAR((i + 1.f) * SIZE, rowVector.sum(), 1e-6);
+		EXPECT_NEAR((i + 1.f) * SIZE, rowVector.sum(), 5e-6);
 		Vector columnVector = a.col(i);
-		EXPECT_NEAR(SIZE * (SIZE+1) / 2., columnVector.sum(), 1e-6);
+		EXPECT_NEAR(SIZE * (SIZE+1) / 2., columnVector.sum(), 5e-6);
 	}
 
 	for (int i = 0;  i < SIZE;  ++i)
@@ -509,9 +509,9 @@
 	for (int i = 0;  i < SIZE;  ++i)
 	{
 		Vector columnVector = a.col(i);
-		EXPECT_NEAR((i + 1.f) * SIZE, columnVector.sum(), 1e-6);
+		EXPECT_NEAR((i + 1.f) * SIZE, columnVector.sum(), 5e-6);
 		Vector rowVector = a.row(i);
-		EXPECT_NEAR(SIZE * (SIZE+1) / 2., rowVector.sum(), 1e-6);
+		EXPECT_NEAR(SIZE * (SIZE+1) / 2., rowVector.sum(), 5e-6);
 	}
 }
 
@@ -529,11 +529,11 @@
 		Vector diagonalVector = Vector::Constant(2.f);
 		a.diagonal() = diagonalVector;
 	}
-	EXPECT_NEAR(2.f * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(2.f * SIZE, a.sum(), 5e-6);
 
 	Matrix b = Matrix::Identity();
 	{
-		EXPECT_NEAR(1.f * SIZE, b.diagonal().sum(), 1e-6);
+		EXPECT_NEAR(1.f * SIZE, b.diagonal().sum(), 5e-6);
 	}
 }
 
@@ -636,9 +636,9 @@
 	const int SIZE = Matrix::RowsAtCompileTime;
 
 	Matrix a = Matrix::Constant(0.1f);
-	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 5e-6);
 	Matrix b = -a;
-	EXPECT_NEAR(-0.1 * SIZE * SIZE, b.sum(), 1e-6);
+	EXPECT_NEAR(-0.1 * SIZE * SIZE, b.sum(), 5e-6);
 }
 
 /// Addition.
@@ -648,9 +648,9 @@
 	const int SIZE = Matrix::RowsAtCompileTime;
 
 	Matrix a = Matrix::Constant(0.1f);
-	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 5e-6);
 	Matrix b = a + Matrix::Ones() + a;
-	EXPECT_NEAR(1.2 * SIZE * SIZE, b.sum(), 1e-6);
+	EXPECT_NEAR(1.2 * SIZE * SIZE, b.sum(), 5e-6);
 }
 
 /// Subtraction.
@@ -660,9 +660,9 @@
 	const int SIZE = Matrix::RowsAtCompileTime;
 
 	Matrix a = Matrix::Constant(0.1f);
-	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 5e-6);
 	Matrix b = Matrix::Ones() - a;
-	EXPECT_NEAR(0.9 * SIZE * SIZE, b.sum(), 1e-6);
+	EXPECT_NEAR(0.9 * SIZE * SIZE, b.sum(), 5e-6);
 }
 
 /// Incrementing by a value.
@@ -672,9 +672,9 @@
 	const int SIZE = Matrix::RowsAtCompileTime;
 
 	Matrix a = Matrix::Constant(0.1f);
-	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 5e-6);
 	a += Matrix::Ones();
-	EXPECT_NEAR(1.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(1.1 * SIZE * SIZE, a.sum(), 5e-6);
 }
 
 /// Decrementing by a value.
@@ -684,9 +684,9 @@
 	const int SIZE = Matrix::RowsAtCompileTime;
 
 	Matrix a = Matrix::Constant(1.1f);
-	EXPECT_NEAR(1.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(1.1 * SIZE * SIZE, a.sum(), 5e-6);
 	a -= Matrix::Ones();
-	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 1e-6);
+	EXPECT_NEAR(0.1 * SIZE * SIZE, a.sum(), 5e-6);
 }
 
 /// Matrix-scalar multiplication.
@@ -696,7 +696,7 @@
 
 	Matrix a = Matrix::Random();
 	Matrix b = a * 1.23f;
-	EXPECT_NEAR(1.23 * a.sum(), b.sum(), 1e-6);
+	EXPECT_NEAR(1.23 * a.sum(), b.sum(), 5e-6);
 }
 
 /// Scalar-matrix multiplication.
@@ -706,7 +706,7 @@
 
 	Matrix a = Matrix::Random();
 	Matrix b = 1.23f * a;
-	EXPECT_NEAR(1.23 * a.sum(), b.sum(), 1e-6);
+	EXPECT_NEAR(1.23 * a.sum(), b.sum(), 5e-6);
 }
 
 /// Division by scalar.
@@ -716,7 +716,7 @@
 
 	Matrix a = Matrix::Random();
 	Matrix b = a / 1.23f;
-	EXPECT_NEAR(a.sum() / 1.23, b.sum(), 1e-6);
+	EXPECT_NEAR(a.sum() / 1.23, b.sum(), 5e-6);
 }
 
 /// Matrix-vector multiplication.
@@ -732,7 +732,7 @@
 	Vector v = Vector::Zero();
 	v[0] = 1.f;
 	Vector w = a * v;
-	EXPECT_NEAR(a.col(0).sum(), w.sum(), 1e-6);
+	EXPECT_NEAR(a.col(0).sum(), w.sum(), 5e-6);
 }
 
 /// Matrix-matrix multiplication.
@@ -797,8 +797,8 @@
 	Matrix a = Matrix::Random();
 	Matrix b = Matrix::Identity();
 	Matrix c = a.cwiseProduct(b);
-	EXPECT_NEAR(0, c.sum() - c.diagonal().sum(), 1e-6);
-	EXPECT_NEAR(0, (a - c).diagonal().sum(), 1e-6);
+	EXPECT_NEAR(0, c.sum() - c.diagonal().sum(), 5e-6);
+	EXPECT_NEAR(0, (a - c).diagonal().sum(), 5e-6);
 }
 
 /// Component-wise division.
@@ -809,7 +809,7 @@
 	Matrix a = Matrix::Random();
 	Matrix b = Matrix::Constant(0.5f);
 	Matrix c = a.cwiseQuotient(b);
-	EXPECT_NEAR(a.sum() * 2, c.sum(), 1e-6);
+	EXPECT_NEAR(a.sum() * 2, c.sum(), 5e-6);
 }
 
 /// Frobenius norm and its square.
@@ -893,7 +893,7 @@
 
 	Matrix a = Matrix::Random();
 	T expectedTrace = a.diagonal().sum();
-	EXPECT_NEAR(expectedTrace, a.trace(), 1e-6);
+	EXPECT_NEAR(expectedTrace, a.trace(), 5e-6);
 }
 
 /// Determinant.
@@ -914,7 +914,7 @@
 
 	Matrix22 m = Matrix22::Random();
 	T expectedDeterminant = m(0, 0) * m(1, 1) - m(0, 1) * m(1, 0);
-	EXPECT_NEAR(expectedDeterminant, m.determinant(), 1e-6);
+	EXPECT_NEAR(expectedDeterminant, m.determinant(), 5e-6);
 }
 
 /// Determinant (explicit 3x3 result).
@@ -925,7 +925,7 @@
 
 	Matrix33 m = Matrix33::Random();
 	T expectedDeterminant = m.row(0).cross(m.row(1)).dot(m.row(2));
-	EXPECT_NEAR(expectedDeterminant, m.determinant(), 1e-6);
+	EXPECT_NEAR(expectedDeterminant, m.determinant(), 5e-6);
 }
 
 // ==================== SUBMATRICES (EXTENDING/SHRINKING) ====================
@@ -949,7 +949,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix3.template block<2, 2>(0, 0) = matrix2;
 
-	EXPECT_NEAR(6.0, matrix3.sum(), 1e-6) << "extending was incorrect: " << matrix3;
+	EXPECT_NEAR(6.0, matrix3.sum(), 5e-6) << "extending was incorrect: " << matrix3;
 }
 
 /// Extending matrices using the block(i,j,r,c) syntax.
@@ -968,7 +968,7 @@
 	Matrix33 matrix3 = Matrix33::Identity();
 	matrix3.block(0, 0, 2, 2) = matrix2;
 
-	EXPECT_NEAR(6.0, matrix3.sum(), 1e-6) << "extending was incorrect: " << matrix3;
+	EXPECT_NEAR(6.0, matrix3.sum(), 5e-6) << "extending was incorrect: " << matrix3;
 }
 
 /// Shrinking matrices using the block<r,c>() syntax.
@@ -991,7 +991,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix2 = matrix3.template block<2, 2>(0, 0);
 
-	EXPECT_NEAR(5.2, matrix2.sum(), 1e-6) << "shrinking was incorrect: " << matrix2;
+	EXPECT_NEAR(5.2, matrix2.sum(), 5e-6) << "shrinking was incorrect: " << matrix2;
 }
 
 /// Extending matrices using the block<r,c>() syntax.
@@ -1013,7 +1013,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix3.template block<2, 2>(0, 0) = matrix2;
 
-	EXPECT_NEAR(6.0, matrix3.sum(), 1e-6) << "extending was incorrect: " << matrix3;
+	EXPECT_NEAR(6.0, matrix3.sum(), 5e-6) << "extending was incorrect: " << matrix3;
 }
 
 /// Shrinking matrices using the block<r,c>() syntax.
@@ -1036,7 +1036,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix2 = matrix3.template block<2, 2>(0, 0);
 
-	EXPECT_NEAR(5.2, matrix2.sum(), 1e-6) << "shrinking was incorrect: " << matrix2;
+	EXPECT_NEAR(5.2, matrix2.sum(), 5e-6) << "shrinking was incorrect: " << matrix2;
 }
 
 /// Extending matrices using the block<r,c>() syntax.
@@ -1059,7 +1059,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix4.template block<3, 3>(0, 0) = matrix3;
 
-	EXPECT_NEAR(14.5, matrix4.sum(), 1e-6) << "extending was incorrect: " << matrix4;
+	EXPECT_NEAR(14.5, matrix4.sum(), 5e-6) << "extending was incorrect: " << matrix4;
 }
 
 /// Shrinking matrices using the block<r,c>() syntax.
@@ -1083,7 +1083,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix3 = matrix4.template block<3, 3>(0, 0);
 
-	EXPECT_NEAR(14.4, matrix3.sum(), 1e-6) << "shrinking was incorrect: " << matrix3;
+	EXPECT_NEAR(14.4, matrix3.sum(), 5e-6) << "shrinking was incorrect: " << matrix3;
 }
 
 /// Extending matrices using the block<r,c>() syntax.
@@ -1106,7 +1106,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix4.template block<3, 3>(0, 0) = matrix3;
 
-	EXPECT_NEAR(14.5, matrix4.sum(), 1e-6) << "extending was incorrect: " << matrix4;
+	EXPECT_NEAR(14.5, matrix4.sum(), 5e-6) << "extending was incorrect: " << matrix4;
 }
 
 /// Shrinking matrices using the block<r,c>() syntax.
@@ -1130,7 +1130,7 @@
 	// class; you don't need to do this in a non-template context.
 	matrix3 = matrix4.template block<3, 3>(0, 0);
 
-	EXPECT_NEAR(14.4, matrix3.sum(), 1e-6) << "shrinking was incorrect: " << matrix3;
+	EXPECT_NEAR(14.4, matrix3.sum(), 5e-6) << "shrinking was incorrect: " << matrix3;
 }
 
 // ==================== TYPE CONVERSION ====================
@@ -1151,9 +1151,9 @@
 	// triggered because the test is a part of a template class; you don't
 	// need to do this in a non-template context.
 	Matrixd d = a.template cast<double>();
-	EXPECT_NEAR(expectedSum, d.sum(), 1e-6);
+	EXPECT_NEAR(expectedSum, d.sum(), 5e-6);
 	Matrixf f = a.template cast<float>();
-	EXPECT_NEAR(expectedSum, f.sum(), 1e-6);
+	EXPECT_NEAR(expectedSum, f.sum(), 5e-6);
 }
 
 // ==================== MISCELLANEOUS ====================
