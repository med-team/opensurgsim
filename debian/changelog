opensurgsim (0.7.0-12) UNRELEASED; urgency=medium

  [ Pino Toscano ]
  * Use /usr/share/dpkg/architecture.mk in debian/rules to make sure to have
    the $DEB_HOST_* variables.
  * Align non-Linux i386 architectures with Linux/i386, as the problems are the
    same:
    - append -ffloat-store to the CXXFLAGS
    - pass -DEIGEN_ALIGNMENT=ON to cmake

  [ Andreas Tille ]
  * d/watch: point to tags, link to issue asking for release tags
  TODO: Build issue with recent libeigen:
      https://github.com/simquest/opensurgsim/issues/5
      https://bugs.launchpad.net/ubuntu/+source/opensurgsim/+bug/1959839

 -- Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>  Fri, 10 Sep 2021 07:29:36 +0200

opensurgsim (0.7.0-11) unstable; urgency=medium

  [ Adrian Bunk ]
  * Team upload.
  * fix FTBFS on i386
         12 - SurgSimMathTest (Failed)
  * fix/workaround FTBFS on mipsel
    cc1plus: out of memory allocating 5399924 bytes after a total of
    73105408 bytes
  * fix/workaround FTBFS on mips64el
    E: Build killed with signal TERM after 150 minutes of inactivity
  * stop ignoring test failures on armhf, I haven't seen the error
    in any recent build logs
    Closes: #962041

  [ Andreas Tille ]
  * Install shaders and textures

 -- Adrian Bunk <bunk@debian.org>  Tue, 02 Jun 2020 14:47:05 +0200

opensurgsim (0.7.0-10) unstable; urgency=medium

  * Team Upload.
  * Convert .linktree to corresponding .links file (Closes: #960496)
  * Fix with cme
  * s/Desription/Description/
  * Add "Rules-Requires-Root:no"

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 25 May 2020 11:14:41 +0530

opensurgsim (0.7.0-9) unstable; urgency=medium

  * Team upload
  * Fix gcc 9 build Closes: #925797

 -- Olivier Sallou <osallou@debian.org>  Sun, 09 Feb 2020 17:22:37 +0000

opensurgsim (0.7.0-8) unstable; urgency=medium

  * Team upload.
  * Point Vcs fields to salsa.debian.org
  * debhelper 12
  * Standards-Version: 4.3.0
  * Secure URI in copyright format
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/copyright
  * Do not run test SurgSimGraphicsTest which fails for strange reasons
    Avoids bug #912110
  * Adapt to libyaml-cpp 0.6.2.patch
  * Multiarch

 -- Andreas Tille <tille@debian.org>  Tue, 29 Jan 2019 15:30:29 +0100

opensurgsim (0.7.0-7) unstable; urgency=medium

  * Team upload.

  [ Steve Langasek ]
  * Fix for build failure with boost 1.65

  [ Andreas Tille ]
  * Standards-Version: 4.1.3
  * debhelper 11
  * Install NOTICE file

 -- Andreas Tille <tille@debian.org>  Tue, 20 Mar 2018 11:28:00 +0100

opensurgsim (0.7.0-6) unstable; urgency=medium

  * Team upload

  [ Fabian Klötzl ]
  * ignore failing unit tests
    Closes: #853599

  [ Andreas Tille ]
  * Standards-Version: 4.1.2

 -- Fabian Klötzl <kloetzl@evolbio.mpg.de>  Thu, 07 Dec 2017 13:53:35 +0100

opensurgsim (0.7.0-5) unstable; urgency=medium

  * Team upload
  * Do not fail on build time tests on armhf where test results would need
    specific adjustment of tolerance otherwise
    Closes:  #847321

 -- Andreas Tille <tille@debian.org>  Thu, 08 Dec 2016 11:22:10 +0100

opensurgsim (0.7.0-4) unstable; urgency=medium

  * Team upload.

  [ Ghislain Antony Vaillant ]
  * Fix FTBFS on architectures where ASSERT_DEATH is not supported.
    - New patch Assert-death-only-if-supported.patch.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 05 Dec 2016 22:09:36 +0000

opensurgsim (0.7.0-3) unstable; urgency=medium

  * Team upload.

  [ Ghislain Antony Vaillant ]
  * Fix FTBFS when building with newest googletest.
    - Build depends on new unified googletest package.
    - Inject path to googlemock via GOOGLEMOCK_DIR.
    - Exclude gtest / gmock artefacts from install.
    Thanks to Lucas Nussbaum for reporting (Closes: #844874)

  [ Andreas Tille ]
  * debhelper 10
  * d/watch: version=4

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 05 Dec 2016 08:49:16 +0000

opensurgsim (0.7.0-2) unstable; urgency=medium

  * Disable BasicThreadTest.RealTimings
  * Only build multiaxis on linux kernels
  * Disable Thread Pool on armel

 -- Paul Novotny <paul@paulnovo.us>  Tue, 25 Oct 2016 16:49:05 -0400

opensurgsim (0.7.0-1) unstable; urgency=medium

  * New upstream version 0.7.0
  * Remove patches that have been applied upstream
  * Refresh old patches
  * Disable Eigen alignment on arm64, armel, armhf (Closes: #838270)
  * Update copyright years
  * Add patch to fix Hurd builds
  * Fixes FTBFS (test failure) on arm64 (Closes: #838271)
  * Fixes strict epsilons in MatrixTests (Closes: #838272)
  * Add hardening +bindnow
  * Add needed Depends to libopensurgsim-dev

 -- Paul Novotny <paul@paulnovo.us>  Sat, 15 Oct 2016 12:38:54 -0400

opensurgsim (0.6.0-6) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload
  * cme fix dpkg-control
  * Fix FTBFS with GCC 6 (thanks for the patch to Graham Inggs
    <ginggs@debian.org>)
    Closes: #831096

  [ Paul Novotny ]
  * Fix FTBFS with eigen3 3.3~beta2-1 (Closes: #835417)

 -- Andreas Tille <tille@debian.org>  Thu, 08 Sep 2016 22:39:37 +0200

opensurgsim (0.6.0-5) unstable; urgency=medium

  * Fix unit test timeouts on multiple archs
  * Disables two sensitive unit tests

 -- Paul Novotny <paul@paulnovo.us>  Sun, 15 Nov 2015 08:57:33 -0500

opensurgsim (0.6.0-4) unstable; urgency=medium

  * Fixes for eigen 3.3-alpha1 (Closes: #803145)
  * Disables eigen alignment on ppc64el and ppc64

 -- Paul Novotny <paul@paulnovo.us>  Sun, 08 Nov 2015 15:37:31 -0500

opensurgsim (0.6.0-3) unstable; urgency=low

  * Disables eigen alignment on i386 and powerpc (Closes: #798693)
  * Fixes TimerTest failures on mips, mipsel, and s390x (Closes: #798718)
  * Fixes unit tests off on some architectures (Closes: #798717)
  * Fixes FEM ply reading on s390x and ppc64 (Closes: #798715)

 -- Paul Novotny <paul@paulnovo.us>  Sun, 20 Sep 2015 12:30:15 -0400

opensurgsim (0.6.0-2) unstable; urgency=low

  [ Paul Novotny ]
  * Updates for yaml-cpp 0.5.2
  * Backport fixes for compiling with GCC 5
  * Remove unneeded unit test libraries from package

  [ Andreas Tille ]
  * Fix Vcs fields via `cme fix dpkg-control`

 -- Paul Novotny <paul@paulnovo.us>  Thu, 10 Sep 2015 09:04:24 -0400

opensurgsim (0.6.0-1) experimental; urgency=low

  * Initial release (Closes: #775753)

 -- Paul Novotny <paul@paulnovo.us>  Thu, 22 Jan 2015 17:33:51 -0500
